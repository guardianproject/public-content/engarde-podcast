---
layout: post
title: 'ProofMode for Indigenous Rights in Mexico'
date: 2024-02-09 10:00:00
description: We invited team members of the "El Sur Resiste" movement to the Guardian Project podcast to share with us their experiences using ProofMode to document the negative impact of the Mayan Train development in southern states of Mexico.
summary: We invited team members of the "El Sur Resiste" movement to the Guardian Project podcast to share with us their experiences using ProofMode to document the negative impact of the Mayan Train development in southern states of Mexico.
author: Fabiola
tag: baseline
categories: [baseline, mexico]
tags: baseline
file: https://guardianproject.info/podcast-audio/ProofModeCaravan-English-Feb2024.mp3
duration: 14467565
length: "30:08"
explicit: "no" 
keywords: "proofmode, mexico, misinformation, disinformation, indigenous rights, provenance, authentication"
block: "no" 
voices: "Fabiola, Armando, Nicolas"
---

We invited team members of the [El Sur Resiste](https://www.elsurresiste.org/) movement to the [Guardian Project podcast](https://guardianproject.info/podcast/) to share with us their experiences using [ProofMode](https://proofmode.org) to document the negative impact of the Mayan Train development in southern states of Mexico.

_Invitamos a miembros del equipo del movimiento El Sur Resiste al podcast del proyecto Guardian para compartir con nosotros sus experiencias utilizando ProofMode para documentar el impacto negativo del desarrollo del Tren Maya en los estados del sur de México._

We were joined by Nicolas and Armando from the [Laboratorio de Medios Libres](https://laboratoriodemedios.org/) who were in charge of the communications logistics of the caravan. Using [ProofMode](https://proofmode.org), they documented their travels and the many environmental and social issues being caused by the Mayan Train development project. They contributed their verifiable photos and videos to [ProofMode Baseline](/baseline) for preservation and storage on the [IPFS](https://ipfs.io) and [Filecoin](https://filecoin.io) decentralized and immutable storage networks.

_Nos acompañaron Nicolás y Armando del Laboratorio de Medios Libres, quienes estaban a cargo de la logística de comunicaciones de la caravana. Utilizando ProofMode, documentaron sus viajes y los numerosos problemas ambientales y sociales causados por el proyecto de desarrollo del Tren Maya. Contribuyeron con sus fotos y videos verificables a ProofMode Baseline para su preservación y almacenamiento en las redes de almacenamiento descentralizadas e inmutables IPFS y Filecoin._

You can watch the [original video interview in Spanish on YouTube](https://www.youtube.com/watch?v=ENYI-GLYbEQ&embeds_referring_euri=https%3A%2F%2Fproofmode.org%2F&source_ve_path=MjM4NTE&feature=emb_title).
