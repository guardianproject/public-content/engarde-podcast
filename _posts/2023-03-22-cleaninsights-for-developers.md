---
layout: post
title: "Clean Insights for Developers"
date: 2023-03-22 12:30:00 -500
file: https://guardianproject.info/podcast-audio/cleaninsightsfordevelopers.mp3
description: "How to integrate Clean Insights into your app"
summary: "How to integrate Clean Insights into your app"
duration: 659632147 
length: "21:44"
explicit: "no" 
keywords: "cleaninsights, analytics, privacy"
block: "no" 
voices: "John"

---

### About this Episode

Learn how to collect analytics on your apps and websites without compromising user privacy.  Clean Insights is an analytics framework from Guardian Project.

This talk covers the technical aspects of integrating Clean Insights which SDKs we have, how to integrated it into your application, and how to see the data you've collected. 

View and/or download the slides from this talk through [Clean Insights Design Talks on Gitlab](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/talks/Clean%20Insights%20for%20Developers%202023.pdf)

A video version is [also available on youtube](https://www.youtube.com/watch?v=EOIpujwOIGo)

### Show Notes and Links

- [Clean Insights Website](https://cleaninsights.org)
- [Guardian Project contact info](https://guardianproject.info/contact/)
- [Video of Android implementation](https://www.youtube.com/watch?v=NT6LmCTpQ4k)
