---
layout: post
title: "Clean Insights: How to Measure Círculo, an app built on Safety, Security, and Privacy?"
date: 2020-06-11 9:30:00 -400
file: https://guardianproject.info/podcast-audio/circulo-draw-with-me-final.mp3
summary: "Another #drawDataWithMe session from our Clean Insights symposium, talking through what possible ways we could add ethical measurements to Circulo, an app built to protect physical safety and security for women journalists in Mexico"
description: "Another #drawDataWithMe session from our Clean Insights symposium, talking through what possible ways we could add ethical measurements to Circulo, an app built to protect physical safety and security for women journalists in Mexico"
duration: "55:11" 
length: "48417935"
explicit: "no" 
keywords: "physical safety, journalists, mexico, circulo, panic button"
block: "no" 
voices: "Fabiola Maurice (GP), Vladimir Cortez (A19), Martha Tudon (A19), Carrie Winfrey (Okthx), Tiffany Robertson (Okthx), Nathan Freitas (GP)"
---

### About this Episode

In this episode, the teams from Guardian Project, Okthanks and Article 19 Mexico talk about Circulo, a collaborative effort to develop a digital tool for journalists in Mexico to join secure, private circles of support to defend against violence and harassment both online and offline. The project, currently available as an Android app in beta, is now considering how it might enable a Clean Insights style of app measurement and analytics, and if so to what ends and purpose? What is the beneficial insight to be gained, and is it worth it considering the additional risks users might be exposed to with additional data resident on their devices?

[Organisms by Chad Crouch](https://freemusicarchive.org/music/Chad_Crouch/Arps/Organisms) is licensed under a Attribution-NonCommercial 3.0 International License. 

### Show Notes and Links

- [Previous Podcast Episode on Circulo](https://guardianproject.info/podcast/2020/episode6-circulo.html)
- [Clean Insights Symposium Extraordinaire](https://cleaninsights.org)
- [Article 19](https://article19.org)
- [Circulo Website](https://encirculo.org/index.en.html)
- [Circulo video tutorial - English](https://www.youtube.com/watch?v=zxOxkud3nVw&feature=emb_title)
- [Circulo Android Open-Source Project](https://gitlab.com/circuloapp/circulo-android)


