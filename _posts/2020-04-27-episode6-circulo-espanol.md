---
layout: post
title: "Círculo: Juntos nos mantenemos Seguros"
date: 2020-04-27 11:00:00 -500
file: https://guardianproject.info/podcast-audio/engard-circulo-april2020-espanol.mp3
summary: "En este episodio, Fabby, encargada de las Relaciones Comunitarias, platica con Vladimir Cortez y Martha Tudon de la oficina de Articulo 19 en Mexico para hablar de Circulo"
description: "En este episodio, Fabby, encargada de las Relaciones Comunitarias, platica con Vladimir Cortez y Martha Tudon de la oficina de Articulo 19 en Mexico para hablar de Circulo"
duration: "15:36" 
length: "1735145"
explicit: "no" 
keywords: "physical safety, jouranlists, mexico, circulo, panic button"
block: "no" 
voices: "Fabiola Maurice, Vladimir Cortez, Martha Tudon"
---

### Sobre este episodio

En este episodio, Fabby, encargada de las Relaciones Comunitarias, platica con Vladimir Cortez y Martha Tudon de la oficina de Articulo 19 en Mexico para hablar de Circulo, un esfuerzo colaborativo entre OkThanks, Articulo 19 y Guardian Project para crear una herramienta digital que ayude a las mujeres periodistas en Mexico a crear redes de apoyo, reenforzar sus protocolos de seguridad y al mismo tiempo ser una herramienta confiable, que pueda garantizar su privacidad, anonimidad y transparencia de datos. Este fue un projecto de dos años de duracion en el que trabajamos de la mano con los usuarios finales a travez de grupos de mensajeria y ademas varios talleres en persona para asegurarnos que todo lo que se implemento en la aplicacion se adaptaba a lo que ellas necesitaban. Escucha el audio para aprender mas sobre este projecto, los retos que enfrentamos y que es lo que viene en el futuro.

Mira el video en el canal de YouTube de [Guardian Project](https://www.youtube.com/watch?v=rhwCLYi6j7M)

![fabby](https://guardianproject.info/podcast-audio/engarde-circulo.jpg)

### Notas del episodio

- [Article 19](https://article19.org)
- [Circulo Website](https://encirculo.org/)
- [Circulo video tutorial](https://www.youtube.com/watch?v=tiEmY5ktnWM&feature=emb_title)
- [Circulo Android Open-Source Project](https://gitlab.com/circuloapp/circulo-android)


