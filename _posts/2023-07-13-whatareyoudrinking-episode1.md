---
layout: post
title: "What Are You Drinking?! Episode #1"
date: 2023-07-13 12:30:00 -500
file: https://guardianproject.info/podcast-audio/WhatAreYouDrinking-CleanInsightsWeeklyPodcast-Episode1.mp3
description: "The Clean Insights Weekly Podcast - Episode 1"
summary: "The Clean Insights Weekly Podcast - Episode 1"
duration: 20704796
length: "21:33"
explicit: "no" 
keywords: "cleaninsights, analytics, privacy"
block: "no" 
voices: "John, Tiff, Nathan, Alfred"

---

### About this Episode

Time for some summer fun, news, and shared drinking of delicious beverages, especially Insightful Bean Coffee and Tea. Come join us to hear more about our work on privacy-preserving measurement and respectful analytics with https://cleaninsights.org

A video podcast version is also available at [https://streamyard.com/qyjk58krpnsi](https://streamyard.com/qyjk58krpnsi)

### Show Notes and Links

- [Clean Insights Website](https://cleaninsights.org)
- [Clean Insights Docs](https://docs.cleaninsights.org)
- [Get Your Insightful Beans!](https://cleaninsights.org/beans)
- [Guardian Project contact info](https://guardianproject.info/contact/)
