---
layout: post
title: "What Are You Drinking?! Episode #2"
date: 2023-08-06 12:30:00 -500
file: https://guardianproject.info/podcast-audio/WhatAreYouDrinking-Episode2-July2023.mp3
description: "The Clean Insights Weekly Podcast - Episode 2"
summary: "The Clean Insights Weekly Podcast - Episode 2"
duration: 25294092
length: "31:36"
explicit: "no" 
keywords: "cleaninsights, analytics, privacy"
block: "no" 
voices: "John, Tiff, Nathan, Alfred"

---

### About this Episode

On today's episode, we play "Explain It" - a game that helps us find ways to help different people from different walks of life understand why they might care about Clean Insights.

Come join us to hear more about our work on privacy-preserving measurement and respectful analytics with https://cleaninsights.org

A video podcast version is also available at [https://www.youtube.com/watch?v=lCNQF7JmqRs](https://www.youtube.com/watch?v=lCNQF7JmqRs)

### Show Notes and Links

- [Clean Insights Website](https://cleaninsights.org)
- [Clean Insights Docs](https://docs.cleaninsights.org)
- [Get Your Insightful Beans!](https://cleaninsights.org/beans)
- [Guardian Project contact info](https://guardianproject.info/contact/)
